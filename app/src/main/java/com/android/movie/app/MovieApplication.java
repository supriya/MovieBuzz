package com.android.movie.app;

import android.app.Application;
import android.content.Context;

import com.android.movie.networkutils.VolleyNetworkRequest;


public class MovieApplication  extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        initApplication();
    }

    private void initApplication() {
        VolleyNetworkRequest.init(this);
    }


}