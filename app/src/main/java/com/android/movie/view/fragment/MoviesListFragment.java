package com.android.movie.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.movie.R;
import com.android.movie.view.adapter.MovieListAdapter;
import com.android.movie.model.HomePageItems;
import com.android.movie.utils.AppConstants;
import com.android.movie.model.MovieItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class MoviesListFragment extends MainBaseFragment  {

    private HomePageItems mMoviesListPageItem;
    @BindView(R.id.recycler_movie_list)
    RecyclerView mRecyclerMovieList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(AppConstants.MOVIES_HOME_PAGE_ITEM)) {
                mMoviesListPageItem = (HomePageItems) getArguments().getSerializable(AppConstants.MOVIES_HOME_PAGE_ITEM);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lyt_movie, null);
        ButterKnife.bind(this,view);
        initUI();
        return view;
    }


    private void initUI() {
        mRecyclerMovieList.setHasFixedSize(true);
        mRecyclerMovieList.setLayoutManager(new LinearLayoutManager(getActivity()));
        setAdapter(mMoviesListPageItem.getMovieItemList());
    }

    private void setAdapter(ArrayList<MovieItem> movieItem) {
        MovieListAdapter movieListAdapter = new MovieListAdapter(getActivity(),movieItem);
        mRecyclerMovieList.setAdapter(movieListAdapter);
    }
}
