package com.android.movie.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.movie.R;


/**
 * Created by Supriya A on 7/16/2017.
 */
public class SplashScreenActivity extends AppCompatActivity {
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        launchNextScreen();
    }

    private void launchNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                redirectToHomeScreen();
            }
        }, 3000);
    }

    private void redirectToHomeScreen() {
        Intent intent = new Intent(this, NavigationActivity.class);
        startActivity(intent);
        finish();
    }


}
