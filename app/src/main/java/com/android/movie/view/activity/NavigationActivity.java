package com.android.movie.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;

import com.android.movie.R;
import com.android.movie.utils.AppConstants;
import com.android.movie.view.fragment.HomeFragment;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class NavigationActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private HomeFragment mHomeFragment;
    private String TAG_HOME_FRAGMENT = "homefragment";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.home));
        mFrameLayout.addView(getLayoutInflater().inflate(R.layout.content_main, null));
        setNavigationMenuItem();
        initHome();

    }

    private void initHome() {
        mHomeFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.home_fragment_container, mHomeFragment, TAG_HOME_FRAGMENT).commit();
    }

    private void setNavigationMenuItem() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //Closing drawer on item click
        mDrawerLayout.closeDrawers();

        //Check to see which item was being clicked and perform appropriate action
        switch (item.getItemId()) {
            //Replacing the main content with ContentFragment Which is our Inbox View;
            case R.id.videos:
                if(mHomeFragment != null)mHomeFragment.setFragmentCurrentItem(AppConstants.VIDEOS_POS);
                return true;
            case R.id.images:
                if(mHomeFragment != null) mHomeFragment.setFragmentCurrentItem(AppConstants.IMAGES_POS);
                return true;
            case R.id.milestones:
                if(mHomeFragment != null) mHomeFragment.setFragmentCurrentItem(AppConstants.MILESTONES_POS);
                return true;
            default:
                return true;
        }
    }
}
