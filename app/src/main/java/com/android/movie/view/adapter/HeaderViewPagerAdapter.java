package com.android.movie.view.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.movie.R;
import com.android.movie.utils.AppConstants;
import com.android.movie.model.MovieItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class HeaderViewPagerAdapter extends PagerAdapter {

    @BindView(R.id.image_view)
    ImageView mImageView;
    @BindView(R.id.bottom_view)
    TextView mTitleView;
    private ArrayList<MovieItem> mMovieItem = null;
    private Context mContext;
    private LayoutInflater mLayoutInflater;


    public HeaderViewPagerAdapter(Context context, ViewPager viewPager, ArrayList<MovieItem> urls) {
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
        mMovieItem = urls;

    }

    @Override
    public int getCount() {
        return AppConstants.HEADER_VIEW_RESTRICTED_SIZE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View view = mLayoutInflater.inflate(R.layout.gallery_item_lyt, container, false);
        ButterKnife.bind(this, view);
        initData(position);
        ((ViewPager) container).addView(view);
        return view;

    }

    private void initData(int position) {
        if (mMovieItem != null && mMovieItem.get(position) != null &&
                !TextUtils.isEmpty(mMovieItem.get(position).getPosterPath())) {
            String imageUrl = AppConstants.IMAGE_BASE_URL + mMovieItem.get(position).getPosterPath();

            if (!TextUtils.isEmpty(imageUrl)) {
                imageUrl = imageUrl.trim();
                Picasso.with(mContext).load(imageUrl).into(mImageView);
            }
            mTitleView.setText(mMovieItem.get(position).getTitle());
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        if (view instanceof RelativeLayout) {
            ((RelativeLayout) view).removeAllViews();
        }
        ((ViewPager) container).removeView((View) object);

    }
}

