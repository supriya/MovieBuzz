package com.android.movie.view.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.movie.R;
import com.android.movie.utils.AppCommonUtility;
import com.android.movie.utils.AppConstants;
import com.android.movie.model.MovieItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.itemViewHolder> {

    private ArrayList<MovieItem> mMovieItem;
    private Activity mActivity;
    private int mGridUnit;


    public MovieListAdapter(Activity activity, ArrayList<MovieItem> showList) {
        mMovieItem = showList;
        if (activity != null) {
            mActivity = activity;
            mGridUnit = AppCommonUtility.getScreenGridUnit(activity);
        }
    }

    @Override
    public itemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_movie_item, parent, false);
        itemViewHolder viewHolder = new itemViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final itemViewHolder holder, final int position) {
        final MovieItem movieItem = mMovieItem.get(position);
        if(movieItem != null){
            if(!TextUtils.isEmpty(movieItem.getTitle())){
                //set title
                holder.mTitle.setText(movieItem.getTitle());
            }
            if(!TextUtils.isEmpty(movieItem.getOverview())){
                //set title
                holder.mOverView.setText(movieItem.getOverview());
            }
            if(!TextUtils.isEmpty(movieItem.getPosterPath())){
                String imageUrl = AppConstants.IMAGE_BASE_URL + movieItem.getPosterPath();

                if (!TextUtils.isEmpty(imageUrl)) {
                    imageUrl = imageUrl.trim();
                    Picasso.with(mActivity).load(imageUrl).fit().into(holder.mImageView);
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return mMovieItem.size();
    }


    public class itemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.overview)
        TextView mOverView;
        @BindView(R.id.image_view)
        ImageView mImageView;

        public itemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
