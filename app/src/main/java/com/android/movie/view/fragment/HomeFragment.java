package com.android.movie.view.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.movie.R;
import com.android.movie.model.HomePageItems;
import com.android.movie.model.MovieItem;
import com.android.movie.presenter.HomePresenter;
import com.android.movie.utils.AppConstants;
import com.android.movie.utils.ThemeUtils;
import com.android.movie.view.adapter.HeaderViewPagerAdapter;
import com.android.movie.view.adapter.HomeFragmentAdapter;
import com.android.movie.widgets.CirclePageIndicator;
import com.android.movie.widgets.CustomViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class HomeFragment extends MainBaseFragment implements TabLayout.OnTabSelectedListener{

    private int[] tabIcons = {
            R.drawable.video_selector,
            R.drawable.image_selctor,
            R.drawable.milestone_selector,
    };

    //init views
    @BindView(R.id.view_pager_home)
    CustomViewPager mHomeViewPager;
    @BindView(R.id.tabs_top_bar)
    TabLayout mTabLayout;
    @BindView(R.id.header_view)
    ViewPager viewPager;
    @BindView(R.id.page_indicator)
    CirclePageIndicator mCirclePageIndicator;
    @BindView(R.id.no_network)
    LinearLayout mNoNetworkLyt;
    @BindView(R.id.movie_progressbar)
    ProgressBar mProgressBar;

    private HomeFragmentAdapter mHomeFragmentAdapter;
    private HomePresenter mHomePresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lyt_home, null);
        ButterKnife.bind(this,view);
        initUI();
        return view;
    }

    private void initUI() {
        //TabView controls
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        ThemeUtils.setAppTabLayoutTheme(getActivity(), mTabLayout);
        //init presenter
        mHomePresenter = new HomePresenter(getContext(),this);

        //set status bar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setFragmentCurrentItem(int position){
        if(mHomeFragmentAdapter != null){
            mHomeViewPager.setCurrentItem(position);
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mHomeViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void setAdapter(ArrayList<MovieItem> movieListItems, ArrayList<HomePageItems> defaultHomeItem) {
        HeaderViewPagerAdapter pagerAdapter = new HeaderViewPagerAdapter(getContext(), viewPager, movieListItems);
        viewPager.setAdapter(pagerAdapter);
        mCirclePageIndicator.setViewPager(viewPager);

        //set the pager adapter
        mHomeFragmentAdapter = new HomeFragmentAdapter(getChildFragmentManager(),defaultHomeItem);
        mHomeViewPager.setAdapter(mHomeFragmentAdapter);

        //set tab paper adapter
        mTabLayout.setupWithViewPager(mHomeViewPager);

        //set tab icons
        for(int index =0 ; index <AppConstants.NO_OF_ITEMS ;index++){
            mTabLayout.getTabAt(index).setIcon(tabIcons[index]);
        }

        mTabLayout.addOnTabSelectedListener(this);
    }

    public void networkLytVisibility(int visible) {
        mNoNetworkLyt.setVisibility(visible);
    }

    public void progressViewVisibility(int gone) {
        mProgressBar.setVisibility(gone);
    }
}
