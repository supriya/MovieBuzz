package com.android.movie.view.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.android.movie.view.fragment.ImageFragment;
import com.android.movie.model.HomePageItems;
import com.android.movie.utils.AppConstants;
import com.android.movie.view.fragment.MoviesListFragment;

import java.util.ArrayList;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class HomeFragmentAdapter extends FragmentStatePagerAdapter {

    private static final int NO_OF_TAB = 3;
    private final SparseArray<Fragment> mPageReferences;
    private ArrayList<HomePageItems> mHomePageItems = new ArrayList<>();

    public HomeFragmentAdapter(FragmentManager fm, ArrayList<HomePageItems> homeItemList) {
        super(fm);
        mPageReferences = new SparseArray<Fragment>();
        mHomePageItems =  homeItemList;
    }


    @Override
    public Fragment getItem(int position) {
        HomePageItems homePageItem = mHomePageItems.get(position);
        Bundle args = new Bundle();
        args.putSerializable(AppConstants.MOVIES_HOME_PAGE_ITEM, homePageItem);
        switch (homePageItem.getUrlType()){
            case AppConstants.TYPE_VIDEOS:
                MoviesListFragment topfragment = new MoviesListFragment();
                topfragment.setArguments(args);
                mPageReferences.put(position, topfragment);
                return topfragment;
            case AppConstants.TYPE_IMAGE:
                ImageFragment imageFragment = new ImageFragment();
                imageFragment.setArguments(args);
                mPageReferences.put(position, imageFragment);
                return imageFragment;
            case AppConstants.TYPE_MILESTONES:
                MoviesListFragment upcomingFragment = new MoviesListFragment();
                upcomingFragment.setArguments(args);
                mPageReferences.put(position, upcomingFragment);
                return upcomingFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return NO_OF_TAB;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mHomePageItems.get(position).getTitle();
    }

    public Fragment getFragment(int key) {
        return mPageReferences.get(key);
    }

    public SparseArray<Fragment> getLiveFragments() {
        return mPageReferences;
    }

}