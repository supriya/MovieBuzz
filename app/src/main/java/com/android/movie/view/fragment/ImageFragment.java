package com.android.movie.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.movie.R;
import com.android.movie.view.adapter.ImageListAdapter;
import com.android.movie.utils.AppConstants;
import com.android.movie.model.HomePageItems;
import com.android.movie.model.MovieItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class ImageFragment  extends MainBaseFragment  {

    @BindView(R.id.recycler_movie_list)
    RecyclerView mRecyclerMovieList;
    private HomePageItems mMoviesPageItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(AppConstants.MOVIES_HOME_PAGE_ITEM)) {
                mMoviesPageItem = (HomePageItems) getArguments().getSerializable(AppConstants.MOVIES_HOME_PAGE_ITEM);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lyt_movie, null);
        ButterKnife.bind(this,view);
        initAdapter();
        return view;
    }


    private void initAdapter() {
        mRecyclerMovieList.setHasFixedSize(true);
        mRecyclerMovieList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        setAdapter(mMoviesPageItem.getMovieItemList());
    }

    private void setAdapter(ArrayList<MovieItem> movieItem) {
        ImageListAdapter imageListAdapter = new ImageListAdapter(getActivity(),movieItem);
        mRecyclerMovieList.setAdapter(imageListAdapter);
    }
}
