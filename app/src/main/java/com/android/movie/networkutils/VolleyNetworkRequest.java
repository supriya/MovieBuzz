package com.android.movie.networkutils;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.google.gson.Gson;
import com.android.movie.utils.AppUtility;
import com.android.movie.model.ILCDataModel;

import java.io.File;

/**
 * Created by Supriya A on 7/16/2016.
 */
public class VolleyNetworkRequest {

    private static RequestQueue mRequestQueue;
    private static RequestQueue mImageRequestQueue;
    private static String TAG = VolleyNetworkRequest.class.getSimpleName();
    private static Context mContext;
    private static Gson mGson;

    @VisibleForTesting
    static final String CACHE_DIRECTORY_NAME = "dailynation-volley-cache";
    public static final int TEN_MB = 10 * 1024 * 1024;

    private VolleyNetworkRequest() {}

    /**
     * initialize Volley
     */
    public static void init(Context context) {
        mGson = new Gson();
        mContext = context;
        getRequestQueue(context);
    }

    public static <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue(mContext).add(req);
    }

    public static RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            File volleyCacheDir = new File(context.getCacheDir().getPath() + File.separator
                    + CACHE_DIRECTORY_NAME);

            Cache cache = new DiskBasedCache(volleyCacheDir, (int) AppUtility.diskCacheSizeBytes
                    (volleyCacheDir, TEN_MB));
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }

        return mRequestQueue;
    }

    public static RequestQueue getImageRequestQueue() {

        if (mImageRequestQueue != null) {
            return mImageRequestQueue;
        } else {
            throw new IllegalStateException("Image RequestQueue not initialized");
        }
    }

    public static ILCDataModel getCachedData(String url, Context mContext, ILCDataModel dataModel) {
        ILCDataModel mDataModel = null;
        try {
            Cache cache = getRequestQueue(mContext).getCache();
            Cache.Entry entry = cache.get(url);
            if (entry != null) {
                String jsonString = "";
                if (mGson == null)
                    mGson = new Gson();

                jsonString = new String(entry.data, "UTF-8");
                mDataModel = mGson.fromJson(jsonString, dataModel.getClass());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mDataModel;
    }

    private static String getkey(String url) {
        String prepareUrl = url.replaceAll("[&?]lat.*?(?=&|\\?|$)", "");
        prepareUrl = prepareUrl.replaceAll("[&?]long.*?(?=&|\\?|$)", "");
        return prepareUrl;
    }
}
