
package com.android.movie.networkutils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.android.movie.model.ILCDataModel;
import com.android.movie.model.IllegalCodeError;
import com.android.movie.model.IllegalErrorCode;
import com.android.movie.utils.AppConstants;
import com.android.movie.utils.AppUtility;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class GsonGetRequest extends Request<ILCDataModel> {

    private final Listener<ILCDataModel> mListener;
    private ILCDataModel mDataModel;
    private final Gson mGson;
    private Map<String, String> mHeaders;
    private String TAG = GsonGetRequest.class.getName();
    public static final int MY_SOCKET_TIMEOUT_MS = 60000;
    private String mUrl;
    private String mRequestBody;
    /**
     * Charset for request.
     */
    private static final String PROTOCOL_CHARSET = "utf-8";


    public GsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mUrl = url;
        AppUtility.log(TAG, "Request Url: " + mUrl);
    }

    public GsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model, Map<String, String> headers) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mHeaders = headers;
        mUrl = url;
    }

    public GsonGetRequest(String url, Listener<ILCDataModel> listener, ErrorListener
            errorListener, ILCDataModel model, Map<String, String> headers, String requestBody) {
        super(Method.GET, url, errorListener);
        mListener = listener;
        mDataModel = model;
        mGson = new Gson();
        mHeaders = headers;
        mUrl = url;
        mRequestBody = requestBody;
    }

    @Override
    protected void deliverResponse(ILCDataModel model) {
        mListener.onResponse(model);

    }

    @Override
    public void deliverError(VolleyError error) {
        super.deliverError(error);
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        retryPolicy = new DefaultRetryPolicy(MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        return super.setRetryPolicy(retryPolicy);
    }



    @Override
    protected Response<ILCDataModel> parseNetworkResponse(NetworkResponse response) {
        String jsonString = new String(response.data);
        int statusCode = response.statusCode;
        AppUtility.log(TAG  , "json Response String: " + jsonString+" statusCode:"+response.statusCode);
        try {

            if (statusCode == AppConstants.ERROR_CODE_403) {
                statusCode = AppConstants.ERROR_CODE_401;
            }
            if (statusCode == AppConstants.ERROR_CODE_401 || statusCode == AppConstants.ERROR_CODE_410) {
                throw new IllegalErrorCode(jsonString);
            }
            if (statusCode == AppConstants.ERROR_CODE_499 || statusCode == AppConstants.ERROR_CODE_502
                    || statusCode == AppConstants.ERROR_CODE_503 || statusCode == AppConstants.ERROR_CODE_504) {
                throw new IllegalErrorCode(jsonString);
            }

            if (jsonString == null || jsonString.trim().length() == 0) {
                throw new IllegalStateException();
            }
            try {

                IllegalCodeError errorStatus = new IllegalCodeError();
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());

                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmResult() != null
                        && errorStatus.getStatusError().getmResult().equalsIgnoreCase(AppConstants.FAILURE)) {
                    VolleyError volleyError = new VolleyError(AppConstants.FAILURE_ERROR);
                    volleyError.setUrl(mUrl);
                    if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                        volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                        volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                    }

                    return Response.error(volleyError);
                }

            } catch (Exception ex) {

            }
            return Response.success((ILCDataModel) mGson.fromJson(jsonString, mDataModel.getClass()), getCacheEntry());

        } catch (IllegalErrorCode e) {
            e.printStackTrace();
            VolleyError volleyError = new VolleyError(String.valueOf(statusCode));
            volleyError.setUrl(mUrl);
            IllegalCodeError errorStatus = new IllegalCodeError();
            try {
                errorStatus = mGson.fromJson(jsonString, errorStatus.getClass());
                if (errorStatus != null && errorStatus.getStatusError() != null && errorStatus.getStatusError().getmMessage() != null) {
                    volleyError.setAlertMessage(errorStatus.getStatusError().getmMessage().getMessage());
                    volleyError.setmAlertTitle(errorStatus.getStatusError().getmMessage().getTitle());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return Response.error(volleyError);
        } catch (Exception exception) {
            exception.printStackTrace();
            VolleyError volleyError = new VolleyError(AppConstants.PARSE_ERROR);
            volleyError.setUrl(mUrl);
            return Response.error(volleyError);
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        try {
            return mHeaders != null ? mHeaders : super.getHeaders();
        } catch (AuthFailureError exception) {
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {

            return null;
        }
    }


}
