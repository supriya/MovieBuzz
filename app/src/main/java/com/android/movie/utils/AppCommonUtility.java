package com.android.movie.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by Supriya A on 8/20/2016.
 */
public class AppCommonUtility {

    public static int getOSVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static int getScreenGridUnit(Context context) {
        return getScreenWidth((Activity) context)
                / AppConstants.NUMBER_OF_BOX_IN_ROW;
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        int width;
        if (activity != null && !activity.isFinishing()) {
            activity.getWindowManager().getDefaultDisplay()
                    .getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
        } else {
            width = 0;
        }
        return width;
    }


}
