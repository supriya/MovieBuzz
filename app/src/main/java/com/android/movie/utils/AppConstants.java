package com.android.movie.utils;

/**
 * Created by Supriya A on 8/20/2016.
 */
public class AppConstants {
    public static String BASE_URL = "https://dl.dropbox.com/s/7ins3y3u9lnf9fd/movies_list.json.txt?dl=0" ;
    public static String IMAGE_BASE_URL ="https://image.tmdb.org/t/p/w500";
    public static String MOVIES_HOME_PAGE_ITEM = "intent_home_page_item";
    public static final String TYPE_VIDEOS = "videos";
    public static final String TYPE_MILESTONES = "milestones";
    public static final String TYPE_IMAGE = "images";

    //VOLLEY ERROR CONSTANTS
    public static final int ERROR_CODE_403 = 403;
    public static final int ERROR_CODE_499 = 499;
    public static final int ERROR_CODE_449 = 449;
    public static final int ERROR_CODE_502 = 502;
    public static final int ERROR_CODE_503 = 503;
    public static final int ERROR_CODE_504 = 504;
    public static final int ERROR_CODE_401 = 401;
    public static final int ERROR_CODE_410 = 410;
    public static final int ERROR_CODE_400 = 400;
    public static final String FAILURE = "failure";
    public static final String FAILURE_ERROR = "failure_error";
    public static final String PARSE_ERROR = "parsing_error";

    public static int NUMBER_OF_BOX_IN_ROW = 16;


    //TEXT VIEW FONTS
    public static final String FONT_FAMILY_SANS_SERIF = "sans-serif";
    public static final String FONT_FAMILY_SANS_SERIF_LIGHT = "sans-serif-light";
    public static final String FONT_FAMILY_SANS_SERIF_MEDIUM = "sans-serif-medium";
    public static final String FONT_ROBOTO_LIGHT = "Roboto-Light.ttf";


    public static int HEADER_VIEW_RESTRICTED_SIZE = 4;
    public static final int VIDEOS_POS = 0 ;
    public static final int IMAGES_POS = 1 ;
    public static final int MILESTONES_POS = 2 ;
    public static int NO_OF_ITEMS = 3;
}
