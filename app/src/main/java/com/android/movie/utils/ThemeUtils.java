package com.android.movie.utils;

import android.app.Activity;
import android.support.design.widget.TabLayout;

import com.android.movie.R;

/**
 * Created by Supriya A on 8/20/2016.
 */
public class ThemeUtils {


    public static void setAppTabLayoutTheme(Activity activity, TabLayout tabLayout) {
        if(tabLayout != null) {
            int primaryColorId = R.color.colorTabIndicator;
            int indiacatorColorId = R.color.colorPrimary;
            int normalTextColorId = R.color.grey;
            int selectedTextColorId = R.color.colorPrimary;
            tabLayout.setBackgroundColor(AppUtility.getColor(activity, primaryColorId));
            tabLayout.setSelectedTabIndicatorColor(AppUtility.getColor(activity, indiacatorColorId));
            tabLayout.setTabTextColors(AppUtility.getColor(activity, normalTextColorId), AppUtility.getColor(activity, selectedTextColorId));
        }
    }
}
