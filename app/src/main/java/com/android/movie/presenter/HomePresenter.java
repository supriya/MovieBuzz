package com.android.movie.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.movie.R;
import com.android.movie.model.HomePageItems;
import com.android.movie.model.ILCDataModel;
import com.android.movie.model.MovieItem;
import com.android.movie.model.MovieItemsList;
import com.android.movie.networkutils.GsonGetRequest;
import com.android.movie.networkutils.VolleyNetworkRequest;
import com.android.movie.utils.AppConstants;
import com.android.movie.utils.AppUtility;
import com.android.movie.view.fragment.HomeFragment;

import java.util.ArrayList;

/**
 * Created by Supriya A on 7/16/2017.
 */

public class HomePresenter implements  Response.ErrorListener, Response.Listener<ILCDataModel>{
    private final Context mContext;
    private final HomeFragment mHomeFragment;

    public HomePresenter(Context context, HomeFragment homeFragment) {
        mContext = context;
        mHomeFragment = homeFragment;
        fetchAPI();
    }

    private void fetchAPI() {

        if(AppUtility.isNetworkAvailable(mContext)) {
            mHomeFragment.networkLytVisibility(View.GONE);
            if (!TextUtils.isEmpty(AppConstants.BASE_URL)) {
                VolleyNetworkRequest.getRequestQueue(mContext).add(
                        new GsonGetRequest(AppConstants.BASE_URL, this, this, new MovieItemsList()));
            }
        } else {
            mHomeFragment.networkLytVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {
        //on repose error display alert
        mHomeFragment.progressViewVisibility(View.GONE);
    }

    @Override
    public void onResponse(ILCDataModel response) {
        if(response != null){
            if(response instanceof MovieItemsList) {
                mHomeFragment.progressViewVisibility(View.GONE);
                MovieItemsList movieItemsList = (MovieItemsList) response;
                if(movieItemsList.getMovieListItems() != null &&
                        movieItemsList.getMovieListItems().size() >0) {
                    mHomeFragment.setAdapter(movieItemsList.getMovieListItems(),getDefaultHomeItem(movieItemsList.getMovieListItems()));
                }
            }
        }
    }

    private ArrayList<HomePageItems> getDefaultHomeItem(ArrayList<MovieItem> movieListItems) {
        ArrayList<HomePageItems>  itemList = new ArrayList<>();
        for(int index = 0;index < AppConstants.NO_OF_ITEMS;index++){
            HomePageItems item = new HomePageItems();
            switch (index){
                case AppConstants.VIDEOS_POS:
                    item.setTitle(mContext.getString(R.string.videos_title).toUpperCase());
                    item.setUrlType(AppConstants.TYPE_VIDEOS);
                    break;
                case AppConstants.IMAGES_POS:
                    item.setTitle(mContext.getString(R.string.image_title).toUpperCase());
                    item.setUrlType(AppConstants.TYPE_IMAGE);
                    break;
                case AppConstants.MILESTONES_POS:
                    item.setTitle(mContext.getString(R.string.milestone_title).toUpperCase());
                    item.setUrlType(AppConstants.TYPE_MILESTONES);
                    break;
            }
            item.setMovieItemList(movieListItems);
            itemList.add(item);
        }
        return itemList;

    }

}
