package com.android.movie.model;

import java.util.ArrayList;


public class HomePageItems implements ILCDataModel {


    private String title ;
    private String url;
    private String urlType;
    private ArrayList<MovieItem> movieListItems;

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public void setMovieItemList(ArrayList<MovieItem> movieListItems) {
        this.movieListItems = movieListItems;
    }

    public ArrayList<MovieItem> getMovieItemList() {
        return movieListItems;
    }
}
