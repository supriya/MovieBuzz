package com.android.movie.model;

import com.google.gson.annotations.SerializedName;
public class MovieItem implements ILCDataModel{
    private String title;
    private String overview;


    private String popularity;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("poster_path")
    private String posterPath;

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getPopularity() {
        return popularity;
    }

    public String getPosterPath() {
        return posterPath;
    }



}
