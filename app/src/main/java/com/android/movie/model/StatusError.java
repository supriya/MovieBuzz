package com.android.movie.model;

import com.google.gson.annotations.SerializedName;


public class StatusError implements ILCDataModel {

    private static final long serialVersionUID = 1L;

    @SerializedName("result")
    private String mResult;

    @SerializedName("message")
    private Error mMessage;

    @SerializedName("code")
    private int mCode;

    public String getmResult() {
        return mResult;
    }

    public Error getmMessage() {
        return mMessage;
    }

    public int getmCode() {
        return mCode;
    }


}
