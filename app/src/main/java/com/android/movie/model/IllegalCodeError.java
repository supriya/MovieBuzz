package com.android.movie.model;

import com.google.gson.annotations.SerializedName;


public class IllegalCodeError implements ILCDataModel {


    private static final long serialVersionUID = 1L;

    @SerializedName("error")
    private String mError;

    @SerializedName("status")
    private StatusError mStatusError;


    public StatusError getStatusError() {
        return mStatusError;
    }

    public String getError() {
        return mError;
    }

    public void setStatusError(StatusError statusError) {
        this.mStatusError = statusError;
    }



}
