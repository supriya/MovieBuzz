package com.android.movie.model;

import java.util.ArrayList;

/**
 * Created by Supriya A on 8/20/2016.
 */
public class MovieItemsList implements ILCDataModel {


    private ArrayList<MovieItem> results;

    public ArrayList<MovieItem> getMovieListItems() {
        return results;
    }

}
