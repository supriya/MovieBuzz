package com.android.movie.widgets;

/**
 * Created by Supriya A on 8/20/2016.
 */

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Basically this class ensures Android only loads each font once per instance of your app
 *
 *
 */
public class Typefaces {
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(Context c, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(),
                            assetPath);
                    cache.put(assetPath, t);
                } catch (Exception e) {
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }

}
