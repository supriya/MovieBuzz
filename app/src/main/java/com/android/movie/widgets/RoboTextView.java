
/*
 * Copyright © 2016, National Media Group Ltd. All rights reserved.
 * Written under contract by Robosoft Technologies Pvt. Ltd.
 */

package com.android.movie.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.android.movie.R;
import com.android.movie.utils.AppConstants;
import com.android.movie.utils.AppUtility;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class RoboTextView extends AppCompatTextView {

	public RoboTextView(Context context) {
		super(context);
		if(!isInEditMode())
		init();
	}

	public RoboTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		//init();
		if(!isInEditMode())
		setAttribute(context, attrs);
	}

	public RoboTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		//init();
		if(!isInEditMode())
		setAttribute(context, attrs);
	}

	private void setAttribute(Context context, AttributeSet attrs) {
		int font;
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.RoboTextView);
		try {
			font = ta.getInteger(R.styleable.RoboTextView_fontType, 0);
		} finally {
			ta.recycle();
		}
		if (font != 0) {
			switch (font) {

				case 1:
					if (AppUtility.getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
						setTypeface(Typeface.create(AppConstants.FONT_FAMILY_SANS_SERIF_LIGHT, Typeface.NORMAL));
					} else {
						setTypeface(null, Typeface.NORMAL);
					}

					break;

				case 2:
					if (AppUtility.getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
						setTypeface(Typeface.create(AppConstants.FONT_FAMILY_SANS_SERIF, Typeface.NORMAL));
					} else {
						setTypeface(null, Typeface.NORMAL);
					}

					break;

				case 3:
					if (AppUtility.getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
						setTypeface(Typeface.create(AppConstants.FONT_FAMILY_SANS_SERIF_MEDIUM, Typeface.NORMAL));
					} else {
						setTypeface(null, Typeface.BOLD);
					}

					break;

				case 4:
					if (AppUtility.getOSVersion() >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
						setTypeface(Typeface.create(AppConstants.FONT_FAMILY_SANS_SERIF, Typeface.BOLD));
					} else {
						setTypeface(null, Typeface.BOLD);
					}
					break;


			}
		}
	}

	private void init() {
		if (!isInEditMode()) {

			try {
				// In case of getTypeFace return null or it not equal to
				// required obj then only set Typeface.
				if (getTypeface() == null
						|| (getTypeface() != null && !getTypeface().equals(
								Typefaces.get(getContext(),
										AppConstants.FONT_ROBOTO_LIGHT))))
					setTypeface(Typefaces.get(getContext(),
							AppConstants.FONT_ROBOTO_LIGHT));
			} catch (Exception e) {
				// In case of exception does not set type face.
			}
		}
	}
}

