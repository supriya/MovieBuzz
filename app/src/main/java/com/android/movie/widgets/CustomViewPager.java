package com.android.movie.widgets;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.android.movie.utils.AppCommonUtility;

/**
 * Created by Supriya A on 7/16/2017.
 */
public class CustomViewPager extends ViewPager {


    public CustomViewPager(Context context) {
        super(context);
    }


    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        try {

            if (v != this && ((v instanceof ViewPager && AppCommonUtility.getOSVersion() < android.os.Build.VERSION_CODES.HONEYCOMB))) {
                ViewPager viewPager = (ViewPager) v;
                int currentPage = viewPager.getCurrentItem();
                int size = viewPager.getAdapter().getCount();
                if (currentPage == (size - 1) && dx < 0) {
                    return false;
                } else if (currentPage == 0 && dx > 0) {
                    return false;
                } else {
                    return true;
                }
            } else if (v != this && v instanceof HorizontalListView) {
                HorizontalListView hlListView = (HorizontalListView) v;
                int size = hlListView.getAdapter().getCount();
                int lastVisibleItem = hlListView.getLastVisiblePosition();
                int firstVisiblePosition = hlListView.getFirstVisiblePosition();
                if (lastVisibleItem == (size - 1) && dx < 0 &&
                        hlListView.getChildAt(hlListView.getChildCount() - 1) != null &&
                        hlListView.getChildAt(hlListView.getChildCount() - 1).getRight() <= hlListView.getWidth() ) {
                    return false;
                } else if (firstVisiblePosition == 0 && dx > 0 &&
                        hlListView.getChildAt(0) != null &&
                        hlListView.getChildAt(0).getLeft() >= 0) {
                    return false;
                } else {
                    return true;
                }
                //return true;

            }

            return super.canScroll(v, checkV, dx, x, y);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}